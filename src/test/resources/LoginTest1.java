import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class LoginTest1 {

    WebDriver driver;

//    @Before
//
//    public void beforeTest(){
//        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
//        driver = new ChromeDriver();
//        driver.get("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
//    }


    @DataProvider(name = "logindp")
    public Iterator<Object[]> loginDp ( ) {

        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"" , "" , "This field is mandatory" , "This field is mandatory" });

        dp.add(new String[]{"olivia.vasile@abc.com" , "" , "" , "This field is mandatory" });

        dp.add(new String[]{"" , "Testing1!" , "This field is mandatory" , "" });

        dp.add(new String[]{"olivia.vasile@abc.com" , "Testing1!" , "" , ""});

        return dp.iterator();
    }

    @Test(dataProvider = "logindp")
    public void negativeLoginTest (String username , String password , String userErrMsg , String passErrMsg) {

        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
        driver = new ChromeDriver();
        driver.get("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

        WebElement usernameInput = driver.findElement(By.id("input-23"));
        WebElement passwordInput = driver.findElement(By.id("input-26"));
        WebElement submitButton = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div/div[2]/main/div/div/div/div/form/button/span"));
        usernameInput.clear();
        usernameInput.sendKeys(username);

        passwordInput.clear();
        passwordInput.sendKeys(password);
        submitButton.submit();

        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div/div[2]/main/div/div/div/div/form/div[1]/div/div[2]/div/div/div"));
        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div/div[2]/main/div/div/div/div/form/div[2]/div/div[2]/div/div/div"));


        Assert.assertEquals(err1.getText() , userErrMsg);
        Assert.assertEquals(err2.getText() , passErrMsg);}


//        driver.close();
//    }
    }







