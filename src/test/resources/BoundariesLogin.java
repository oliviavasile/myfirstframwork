import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class BoundariesLogin {

    WebDriver driver;

    @BeforeTest
    public void beforeTest ( ) {
        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();

        driver = new ChromeDriver();
        driver.get("http://86.121.249.149:4999/stubs/auth.html#registration_panel");
        WebElement registration = driver.findElement(By.id("register-tab"));

        registration.click();
    }

    @AfterTest
    public void afterTest ( ) {
        driver.close();
    }

    @DataProvider(name = "bounddp")


    public Iterator<Object[]> bounddp ( ) {
        Collection<Object[]> dp = new ArrayList<Object[]>();

        dp.add(new String[]{
                "A" ,
                "A" ,
                "AAA" ,
                "AAAAAAA" ,
                "AAAAAAA" ,

                "Invalid input. Please enter between 2 and 35 letters" ,
                "Invalid input. Please enter between 2 and 35 letters" ,

                "Invalid input. Please enter between 4 and 35 letters or numbers" ,
                "Invalid input. Please use a minimum of 8 characters" ,
                "Please confirm your password"
        });

        dp.add(new String[]{
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" ,
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" ,
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" ,
                "AAAAAAA" ,
                "AAAAAAA" ,

                "Invalid input. Please enter between 2 and 35 letters" ,
                "Invalid input. Please enter between 2 and 35 letters" ,
                "Invalid input. Please enter between 4 and 35 letters or numbers" ,
                "Invalid input. Please use a minimum of 8 characters" ,
                "Please confirm your password"
        });

        return dp.iterator();
    }

    @Test(dataProvider = "bounddp")
    public void boundariesRegisterTest (String fn , String ln , String userName ,
                                      String password , String password2 ,
                                      String fnErrMsg , String lnErrMsg , String userNameErrMsg ,
                                      String passwordErrMsg , String password2ErrMsg) {
//        WebDriverManager.getInstance(DriverManagerType.CHROME).setup();
//        driver = new ChromeDriver();
//        driver.get("http://86.121.249.149:4999/stubs/auth.html#registration_panel");


        WebElement fnInput = driver.findElement(By.id("inputFirstName"));
        WebElement lnInput = driver.findElement(By.id("inputLastName"));
        WebElement userNameInput = driver.findElement(By.id("inputUsername"));
        WebElement passwordInput = driver.findElement(By.id("inputPassword"));
        WebElement password2Input = driver.findElement(By.id("inputPassword2"));
        WebElement submitButton = driver.findElement(By.id("register-submit"));
//            WebElement registration = driver.findElement(By.id("register-tab"));
//            registration.click();


        fnInput.clear();
        fnInput.sendKeys(fn);

        lnInput.clear();
        lnInput.sendKeys(ln);


        userNameInput.clear();
        userNameInput.sendKeys(userName);


        passwordInput.clear();
        passwordInput.sendKeys(password);

        password2Input.clear();
        password2Input.sendKeys(password2);


        submitButton.submit();

        WebElement err1 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[3]/div/div[2]"));
        WebElement err2 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[4]/div/div[2]"));
        WebElement err4 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[6]/div/div[2]"));
        WebElement err5 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[7]/div/div[2]"));
        WebElement err6 = driver.findElement(By.xpath("//*[@id=\"registration_form\"]/div[8]/div/div[2]"));


        Assert.assertEquals(err1.getText() , fnErrMsg);
        Assert.assertEquals(err2.getText() , lnErrMsg);
        Assert.assertEquals(err4.getText() , userNameErrMsg);
        Assert.assertEquals(err5.getText() , passwordErrMsg);
        Assert.assertEquals(err6.getText() , password2ErrMsg);


//        driver.close();
//
    }
}

