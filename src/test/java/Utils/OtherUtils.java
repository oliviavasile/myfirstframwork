package Utils;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.util.Set;

public class OtherUtils {

    public static void printCookie(Cookie c){
        System.out.println(("Cookie= " + c.getName() + " : " + c.getValue()));
        System.out.println(c.getExpiry());
    }
    public static void printCookies (WebDriver driver) {
        Set<Cookie> cookies = driver.manage().getCookies();
        System.out.println("the  number of cookies found are:" + cookies.size());
        for (Cookie c : cookies) {
           printCookie(c);
        }
    }
}