package Tests;

import PageObject.LoginPage;
import PageObject.ProfilePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class LoginTest extends BaseTest {
    @DataProvider(name = "logindp")
    public Iterator<Object[]> loginDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // username, password, username error message, password error message, general error message
        dp.add(new String[]{"", "", "Please enter your username", "Please enter your password", ""});
        dp.add(new String[]{"aaaa", "", "", "Please enter your password", ""});
        dp.add(new String[]{"", "aaaa", "Please enter your username", "", ""});
        dp.add(new String[]{"aaa", "aaa", "", "", "Invalid username or password!"});
        return dp.iterator();
    }

    @Test(dataProvider = "logindp")
    public void loginNegativeTest(String username, String password, String userErrMsg, String passErrMsg, String generalErrMsg) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);

//        LoginPage loginPage1 = PageFactory.initElements(driver, LoginPage.class);

//       login
        loginPage.login(username, password);

        Assert.assertTrue(loginPage.checkErr(userErrMsg, "userErr"));
        Assert.assertTrue(loginPage.checkErr(passErrMsg, "passErr"));
        Assert.assertTrue(loginPage.checkErr(generalErrMsg, "generalErr"));
    }

    @DataProvider(name = "loginPositiveDp")
    public Iterator<Object[]> loginPositiveDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // username, password
        dp.add(new String[]{"dinosaur", "dinosaurpassword"});
        dp.add(new String[]{"dingo", "dingopassword"});
        dp.add(new String[]{"camel", "camelpassword"});
        dp.add(new String[]{"zebra", "zebrapassword"});
        return dp.iterator();
    }

    @Test(dataProvider = "loginPositiveDp")
    public void loginPositive(String username, String password) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(hostname);

        loginPage.login(username, password);

        ProfilePage profilePage = new ProfilePage(driver);
        System.out.println("Logged in user :" + profilePage.getUser());
        Assert.assertEquals(username, profilePage.getUser());

        profilePage.logOut();
//        wait for login page to be loaded
        loginPage.waitForLoginPage();
    }
}
