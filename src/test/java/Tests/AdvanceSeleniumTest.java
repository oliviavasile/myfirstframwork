package Tests;

import Tests.BaseTest;
import Utils.OtherUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.Set;

public class AdvanceSeleniumTest  extends BaseTest {
    @Test
    public void browserTest ( ) {
        driver.get("https://google.com");
    }

    @Test
    public void cookieTest ( ) {
        driver.get("https://google.com");
        OtherUtils.printCookies(driver);
        try {
            OtherUtils.printCookie(driver.manage().getCookieNamed("CONSENT1"));
        } catch (Exception e) {
            System.out.println("Cookie not found");
        }
        Cookie cookie = new Cookie("my cookie", "1 2 3");
        driver.manage().addCookie(cookie);
        OtherUtils.printCookies(driver);
        driver.manage().deleteCookieNamed("CONSENT");
        OtherUtils.printCookies(driver);
        driver.manage().deleteAllCookies();
        OtherUtils.printCookies(driver);
    }

    @Test
    public void cookieCheckTest(){
        driver.get(hostname + "/stubs/cookie.html");
       OtherUtils.printCookies(driver);
        WebElement setCookie = driver.findElement(By.id("set-cookie"));
        setCookie.click();
        OtherUtils.printCookies(driver);


    }
}
