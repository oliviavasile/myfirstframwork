package Tests;

import PageObject.LoginPage;
import PageObject.ProfilePage;
import PageObject.RegisterPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class RegisterTest extends BaseTest{
//    @BeforeTest
//    public void beforeTest ( ) {
//        WebElement registration = driver.findElement(By.id("register-tab"));
//        registration.click();
//    }

    @DataProvider(name = "registerdp")


    public Iterator<Object[]> registerdp ( ) {
        Collection<Object[]> dp = new ArrayList<Object[]>();

        dp.add(new String[]{
                "" ,
                "" ,
                "" ,
                "" ,
                "" ,
                "" ,
                "Invalid input. Please enter between 2 and 35 letters" ,
                "Invalid input. Please enter between 2 and 35 letters" ,
                "Invalid input. Please enter a valid email address" ,
                "Invalid input. Please enter between 4 and 35 letters or numbers" ,
                "Invalid input. Please use a minimum of 8 characters" ,
                "Please confirm your password"
        });

        dp.add(new String[]{
                "" ,
                "AAA" ,
                "test@test.com" ,
                "Testarescu" ,
                "testing1" ,
                "testing1" ,

                "Invalid input. Please enter between 2 and 35 letters",
                "" ,
                "" ,
                "" ,
                "" ,
                ""
        });

        dp.add(new String[]{
                "AAA",
                "" ,
                "test@test.com" ,
                "Testarescu" ,
                "testing1" ,
                "testing1" ,

                "" ,
                "Invalid input. Please enter between 2 and 35 letters",
                "" ,
                "" ,
                "" ,
                ""
        });

        dp.add(new String[]{
                "AAA" ,
                "AAA" ,
                "" ,
                "Testarescu" ,
                "testing1" ,
                "testing1" ,

                "" ,
                "" ,
                "Invalid input. Please enter a valid email address" ,
                "" ,
                "" ,
                ""
        });

        dp.add(new String[]{
                "AAA" ,
                "AAA" ,
                "test@test.com" ,
                "" ,
                "testing1" ,
                "testing1" ,

                "" ,
                "" ,
                "" ,
                "Invalid input. Please enter between 4 and 35 letters or numbers" ,
                "" ,
                ""
        });
        dp.add(new String[]{
                "AAA" ,
                "AAA" ,
                "test@test.com" ,
                "Testarescu" ,
                "" ,
                "Testing1" ,

                "" ,
                "" ,
                "" ,
                "" ,
                "Invalid input. Please use a minimum of 8 characters" ,
                "Please confirm your password"// to investigate
        });

        dp.add(new String[]{
                "AAA" ,
                "AAA" ,
                "test@test.com" ,
                "Testarescu" ,
                "Testing1" ,
                "" ,

                "" ,
                "" ,
                "" ,
                "" ,
                "" ,
                "Please confirm your password"
        });

        return dp.iterator();
    }

    @Test(dataProvider = "registerdp")
    public void negativeRegisterTest (String fn , String ln ,
                                      String email , String userName ,
                                      String password , String password2 ,
                                      String fnErrMsg , String lnErrMsg ,
                                      String emailErrMsg , String userNameErrMsg ,
                                      String passwordErrMsg , String password2ErrMsg) {
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.openRegisterPage (hostname);


        registerPage.register(fn , ln , email , userName , password , password2);



        Assert.assertTrue(registerPage.checkErr(fnErrMsg, "errFN"));
        Assert.assertTrue(registerPage.checkErr(lnErrMsg, "errLN"));
        Assert.assertTrue(registerPage.checkErr(emailErrMsg, "errEmail"));
        Assert.assertTrue(registerPage.checkErr(userNameErrMsg, "errUN"));
        Assert.assertTrue(registerPage.checkErr(passwordErrMsg, "errPass"));
        Assert.assertTrue(registerPage.checkErr(password2ErrMsg, "errPass2"));

    }

    @DataProvider (name = "registerPositiveDp")
    public Iterator<Object[]> registerPositiveDp(){
        Collection<Object[]> dpr = new ArrayList<Object[]>();
        //fn , ln , email , userName , password , password2
        dpr.add(new String[]{"Olivia", "Vasile", "olivia.vasile@test.com","Olivia.test","Testing1","Testing1"});
        return dpr.iterator();
    }
    @Test( dataProvider = "registerPositiveDp")
    public void registerPositive (String fn , String ln , String email , String userName ,
                                  String password , String password2){
        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.openRegisterPage(hostname);
        registerPage.register(fn , ln , email , userName , password , password2);
    }






}
