package Tests;

import PageObject.CookiesPage;
import PageObject.LoginPage;
import Utils.OtherUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CookiesTest extends BaseTest {
    @Test
    public void CookieTest1 ( ) {
        CookiesPage cookiesPage = new CookiesPage(driver);
        Cookie cookie1 = new Cookie("myCookie", "cookie123");
        cookiesPage.openCookiesPage(hostname);

        cookiesPage.newCookie(cookie1);
        OtherUtils.printCookies(driver);
        Assert.assertEquals(cookie1.getValue(), "cookie123");

        driver.manage().deleteCookie(cookie1);
        cookiesPage.deleteCookie();
        OtherUtils.printCookies(driver);
        String newCookieValue = driver.findElement(By.id("cookie-value")).getText();
        Assert.assertEquals(newCookieValue, "");


    }
}