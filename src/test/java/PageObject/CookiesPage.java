package PageObject;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CookiesPage {

    Cookie cookie1;

    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(id = "set-cookie")
    WebElement setCookie;
    @FindBy (id = "delete-cookie")
    WebElement deleteCookie;
    @FindBy (id = "cookie-value")
    WebElement cookieValue;


    public CookiesPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void openCookiesPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/stubs/cookie.html");
        driver.get(hostname + "/stubs/cookie.html");
    }

    public void newCookie(Cookie cookie){
        driver.manage().addCookie(cookie);

    }


    public void deleteCookie(){
        deleteCookie.click();
    }

  public void printCookieValue(){
      System.out.println(cookieValue);

    }

}
