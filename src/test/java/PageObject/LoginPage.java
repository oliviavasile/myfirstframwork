package PageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;
    WebDriverWait wait;

    @FindBy(how = How.ID, using = "input-login-username")
    WebElement usernameInput;
    @FindBy(id = "input-login-password")
    WebElement passwordInput;
    @FindBy(id = "login-submit")
    WebElement submitButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"login_form\"]/div[2]/div/div[2]")
    WebElement errUserName;
    @FindBy(xpath = "//*[@id=\"login_form\"]/div[3]/div/div[2]")
    WebElement errPassword;
    @FindBy(id = "login-error")
    WebElement errGeneral;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void login(String username, String password) {
        usernameInput.clear();
        usernameInput.sendKeys(username);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        submitButton.submit();
    }

    public void waitForLoginPage() {
        wait.until(ExpectedConditions.elementToBeClickable(submitButton));
    }

//    public void initializeGeneralErrorElement() {
//        errGeneral = SeleniumUtils.waitForGenericElement(driver, By.id("login-error"), 15);
//    }

    public boolean checkErr(String error, String type) {
        if (type.equalsIgnoreCase("userErr"))
            return error.equals(errUserName.getText());
        else if (type.equalsIgnoreCase("passErr"))
            return error.equals(errPassword.getText());
        else if (type.equalsIgnoreCase("generalErr"))
            return error.equals(errGeneral.getText());
        return false;
    }

    public void openLoginPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/stubs/auth.html");
        driver.get(hostname + "/stubs/auth.html");
    }
}

