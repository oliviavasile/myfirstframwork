package PageObject;

import Utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfilePage {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(id="logout-submit")
    WebElement logOutButton;

    By user = By.id("user");

    public ProfilePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void logOut(){
        wait.until(ExpectedConditions.elementToBeClickable(logOutButton)).click();
    }

    public String getUser(){
        return SeleniumUtils.waitForGenericElement(driver,user, 15).getText();
    }
}
